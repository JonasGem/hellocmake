# Projektets namn
project(HelloCMake)
# Minsta tillåtna CMake version
cmake_minimum_required(VERSION 3.2)
# Kompilera med stöd för C++ 11
set (CMAKE_CXX_STANDARD 11)
# Ta med alla källkodsfiler i den aktuella katalogen och
# lägg dem i listan SRC_LIST
aux_source_directory(. SRC_LIST)
# Skapa en exekverbar fil med källkodsfilerna från
# SRC_LIST. Första parametern är namnet på målet (Target)
add_executable(${PROJECT_NAME} ${SRC_LIST} src/main.cpp)